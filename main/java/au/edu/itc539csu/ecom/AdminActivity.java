package au.edu.itc539csu.ecom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import au.edu.itc539csu.ecom.adapters.admin.AdminProductsAdapter;

public class AdminActivity extends AppCompatActivity {

    private Button addMoreButton;


    RecyclerView recyclerView;
    AdminProductsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);



        recyclerView = (RecyclerView) findViewById(R.id.admin_recyclerview);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new AdminProductsAdapter(this);
        adapter.loadData();
        recyclerView.setAdapter(adapter);



        addMoreButton = (Button) findViewById(R.id.admin_add_more_product_button);
        addMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //add more button action
                Intent intent = new Intent(AdminActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //signout
        FirebaseAuth.getInstance().signOut();
        finish();
        return true;
        //--signout

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            // do something on back.
            FirebaseAuth.getInstance().signOut();
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
