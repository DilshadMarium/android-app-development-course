package au.edu.itc539csu.ecom.adapters.admin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import au.edu.itc539csu.ecom.R;
import au.edu.itc539csu.ecom.model.OpenCart;
import au.edu.itc539csu.ecom.model.Product;

/**
 * Created by hkg328 on 9/18/17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {  //same with AdminProductsAdapter


    private Context mContext;
    private List<Product> data = new ArrayList<>();

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public ProductListAdapter(Context context){
        mContext = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_product_cell, parent, false);
        return new RowCell(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        RowCell cell = (RowCell) holder;

        cell.productName.setText(data.get(position).productName);
        cell.productPrice.setText(data.get(position).productPrice + " AUD");
        cell.productSize.setText(data.get(position).productSize);

        Picasso.with(mContext).load(data.get(position).imageUrl).into(cell.productImage);

        cell.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenCart.addToCart(data.get(position));
                Toast.makeText(mContext, "Successfully added to Cart!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        if(data == null)
            return 0;
        return data.size();
    }

    private class RowCell extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productPrice, productSize, productName;
        Button addButton;
        public RowCell(View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.product_productcell_image);

            productPrice = (TextView) view.findViewById(R.id.product_product_price);
            productSize = (TextView) view.findViewById(R.id.product_product_size);
            productName = (TextView) view.findViewById(R.id.product_productcell_name);

            addButton = (Button) view.findViewById(R.id.product_add_to_order);
        }
    }


    public void loadData(){

        DatabaseReference myRef = database.getReference("items");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> pMap = (Map<String, Object>) dataSnapshot.getValue();



                Product newProduct = new Product();
                newProduct.id = dataSnapshot.getKey();
                newProduct.productName = pMap.get("productname").toString();
                newProduct.productPrice = pMap.get("price").toString();
                newProduct.productSize = pMap.get("size").toString();
                newProduct.imageUrl = pMap.get("picture").toString();
                data.add(newProduct);


                notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
