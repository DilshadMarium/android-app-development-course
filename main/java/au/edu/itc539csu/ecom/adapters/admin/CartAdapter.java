package au.edu.itc539csu.ecom.adapters.admin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import au.edu.itc539csu.ecom.R;
import au.edu.itc539csu.ecom.model.OpenCart;
import au.edu.itc539csu.ecom.model.Product;

/**
 * Created by hkg328 on 9/18/17.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {  //this class is for the cart when user pick some products to buy.



    private Context mContext;
    private List<Product> data = new ArrayList<>();

    FirebaseDatabase database = FirebaseDatabase.getInstance();


    public CartAdapter(Context context){
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_cell, parent, false);
        return new RowCell(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        RowCell cell = (RowCell) holder;

        cell.name.setText(data.get(position).productName);
        cell.price.setText(data.get(position).productPrice + " $");


    }

    @Override
    public int getItemCount() {

        if(data == null)
            return 0;
        return data.size();

    }

    private static class RowCell extends RecyclerView.ViewHolder {

        TextView name, price;

        public RowCell(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.cart_cell_pname);
            price = (TextView) view.findViewById(R.id.cart_cell_pprice);
        }
    }

    public void loadData(){

        data = OpenCart.getCartAsArray();
        notifyDataSetChanged();

    }
}
