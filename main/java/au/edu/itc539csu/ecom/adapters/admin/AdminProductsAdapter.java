package au.edu.itc539csu.ecom.adapters.admin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import au.edu.itc539csu.ecom.AddProductActivity;
import au.edu.itc539csu.ecom.R;
import au.edu.itc539csu.ecom.model.Product;

/**
 * Created by hkg328 on 9/18/17.
 */

//this class is for display all the product in admin page.
public class AdminProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Product> data = new ArrayList<>();  //all products list

    FirebaseDatabase database = FirebaseDatabase.getInstance();


    public AdminProductsAdapter(Context context){
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_product_cell, parent, false);
        return new RowCell(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        RowCell cell = (RowCell) holder;

        cell.productName.setText(data.get(position).productName);
        Picasso.with(mContext).load(data.get(position).imageUrl).into(cell.productImage);


        cell.editProduct.setOnClickListener(new View.OnClickListener() {   //this is the action when admin click the edit button for product
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AddProductActivity.class);
                intent.putExtra("isEditMode", true);
                Common.currentProduct = data.get(position);
                mContext.startActivity(intent);
            }
        });
        cell.deleteProduct.setOnClickListener(new View.OnClickListener() {  //this action will be invoked when admin click delete button for deleting product
            @Override
            public void onClick(View v) {
                DatabaseReference myRef = database.getReference("items");
                myRef.child(data.get(position).id).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(mContext, "Successfully deleted from database", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {  //in this method , have to return list size so that display list in admin page.
        // so we have to return the size of data array varialbe.

        if(data == null)
            return 0;
        return data.size();

    }

    private static class RowCell extends RecyclerView.ViewHolder {  //this class represent one product in list
        ImageView productImage;
        TextView productName;
        TextView deleteProduct, editProduct;


        public RowCell(View view) {
            super(view);

            productImage = (ImageView) view.findViewById(R.id.admin_productcell_image);
            productName = (TextView) view.findViewById(R.id.admin_productcell_name);
            deleteProduct = (TextView) view.findViewById(R.id.admin_product_delete);
            editProduct = (TextView) view.findViewById(R.id.admin_product_edit);
        }
    }

    public void loadData(){  //fetch product data from firebase

        DatabaseReference myRef = database.getReference("items");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {  //if product is added, then will receive that action here.
                Map<String, Object> pMap = (Map<String, Object>) dataSnapshot.getValue();

                Product newProduct = new Product();
                newProduct.id = dataSnapshot.getKey();
                newProduct.productName = pMap.get("productname").toString();
                newProduct.productPrice = pMap.get("price").toString();
                newProduct.productSize = pMap.get("size").toString();
                if(pMap.get("picture")!= null)
                    newProduct.imageUrl = pMap.get("picture").toString();
                data.add(newProduct);
                notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {  //if product is changed, then thi saction will be executed.
                String key = dataSnapshot.getKey();
                Map<String, Object> pMap = (Map<String, Object>) dataSnapshot.getValue();

                for(int i=0;i<data.size();i++){
                    if(data.get(i).id.equals(key)){
                        data.get(i).productName = pMap.get("productname").toString();
                        data.get(i).productPrice = pMap.get("price").toString();
                        data.get(i).productSize = pMap.get("size").toString();
                        if(pMap.get("picture")!= null)
                            data.get(i).imageUrl = pMap.get("picture").toString();
                        notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {  //if product is deleted then this aciton will be executed.

                String key = dataSnapshot.getKey();
                for(int i=0;i<data.size();i++){
                    if(data.get(i).id.equals(key)){
                        data.remove(i);
                        notifyDataSetChanged();
                        break;
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


}
