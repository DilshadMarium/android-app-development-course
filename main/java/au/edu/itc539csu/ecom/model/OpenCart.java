package au.edu.itc539csu.ecom.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hkg328 on 9/18/17.
 */

public class OpenCart {
    public static HashMap<String, Product> cart = new HashMap<>();

    public static void addToCart(Product product){
        cart.put(product.id, product);
    }
    public static void clearCart(){
        cart.clear();
    }
    public static int count(){
        return cart.size();
    }
    public static ArrayList<Product> getCartAsArray(){
        return new ArrayList<>(cart.values());
    }

    public static int getTotalPrice(){

        int totalPrice = 0;
        ArrayList<Product> products = getCartAsArray();
        for(int i=0;i<products.size();i++){
            totalPrice += Integer.parseInt(products.get(i).productPrice);
        }
        return totalPrice;
    }
}
