package au.edu.itc539csu.ecom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import au.edu.itc539csu.ecom.model.OpenCart;
import au.edu.itc539csu.ecom.model.Product;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nameText, phoneText, emailText, addressText;
    Button confirmOrderButton;



    private FirebaseDatabase database = FirebaseDatabase.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);


        setupLoadingDlg();

        nameText = (EditText) findViewById(R.id.address_name);
        phoneText = (EditText) findViewById(R.id.address_phone);
        emailText = (EditText) findViewById(R.id.address_email);
        addressText = (EditText) findViewById(R.id.address_address);

        confirmOrderButton = (Button) findViewById(R.id.confirm_order);
        confirmOrderButton.setOnClickListener(this);
    }

    ProgressDialog progressDialog;
    private void setupLoadingDlg(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait a second");
    }

    @Override
    public void onClick(View v) {

        String name = nameText.getText().toString();
        String phone = phoneText.getText().toString();
        String email = emailText.getText().toString();
        String address = addressText.getText().toString();

        //input validation part
        if(name.equals("")){
            Toast.makeText(this, "Please input name!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(phone.equals("")){
            Toast.makeText(this, "Please input phone!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(email.equals("")){
            Toast.makeText(this, "Please input email!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(address.equals("")){
            Toast.makeText(this, "Please input address!", Toast.LENGTH_SHORT).show();
            return;
        }

//making order data to save.
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("name", name);
        data.put("phone", phone);
        data.put("email", email);
        data.put("address", address);

        HashMap<String, Object> content = new HashMap<String, Object>();
        ArrayList<Product> orders = OpenCart.getCartAsArray();
        for(int i=0;i< orders.size(); i++){
            content.put(orders.get(i).id, new Integer(1));
        }


        data.put("order_content", content);
//save order data to database
        DatabaseReference myRef = database.getReference("orders").push().getRef();
        myRef.setValue(data);


        Intent intent = new Intent(this, ThankyouOrderActivity.class);
        startActivity(intent);
    }
}
