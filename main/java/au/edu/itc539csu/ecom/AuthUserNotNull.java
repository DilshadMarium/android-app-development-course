package au.edu.itc539csu.ecom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;


public class AuthUserNotNull extends AppCompatActivity {

    Button logOutbutton ;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;
    int [] IMAGES = {R.drawable.products,R.drawable.aboutus,R.drawable.contactus,R.drawable.location};

    String [] NAMES = {"VIEW PRODUCTS", "ABOUT US", "CONTACT US", "LOCATE US" } ;



    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        mGoogleApiClient.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_user_not_null);
        logOutbutton = (Button) findViewById(R.id.logOutButton);



        ListView menuListview = (ListView)findViewById(R.id.menuListview);
        mAuth = FirebaseAuth.getInstance();

        CustomAdapter customAdapter = new CustomAdapter();
        menuListview.setAdapter(customAdapter);

        menuListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long id) {
                Intent intent = null;
                switch(position) {
                    case 0:
                        intent = new Intent(getApplicationContext(), ProductList.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(),EmptyActivity.class);
                        startActivity(intent);
                        break;

                    case 2:
                        intent = new Intent(getApplicationContext(),EmptyActivity.class);
                        startActivity(intent);
                        break;


                    case 3:
                        intent = new Intent(getApplicationContext(),LocationActivity.class);
                        startActivity(intent);
                        break;
                    default:
                }
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseauth){
                if( firebaseauth.getCurrentUser() == null){

                    startActivity(new Intent(AuthUserNotNull.this,MainActivity.class));
                }
            }
        };




        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(AuthUserNotNull.this,"Something has gone wrong",Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



        logOutbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();

                try {
                    Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);
//                    Auth.GoogleSignInApi.signOut(thisAppGoogleApiClient);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });


    }


    class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.activity_custom_layout,null);
            ImageView menuimage= (ImageView)convertView.findViewById(R.id.imageView);
            TextView menutitle = (TextView)convertView.findViewById(R.id.textView);

            menuimage.setImageResource(IMAGES[position]);
            menutitle.setText(NAMES[position]);
            return convertView;
        }



    };
}
