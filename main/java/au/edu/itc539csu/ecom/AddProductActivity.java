package au.edu.itc539csu.ecom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.UUID;

import au.edu.itc539csu.ecom.adapters.admin.Common;
import au.edu.itc539csu.ecom.model.Product;

import static android.R.attr.data;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "AddProductActivity";
    private ImageView productImage;
    private Uri productImageUri = null;

    private TextView productName, productSize, productPrice;
    private Button addButton;


    private boolean isEditMode = false;  //if true  - add new product, if false - edit product

    private StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        setupLoadingDlg();

        productImage = (ImageView) findViewById(R.id.new_product_image);
        productName = (TextView) findViewById(R.id.new_product_name);
        productSize = (TextView) findViewById(R.id.new_product_size);
        productPrice = (TextView) findViewById(R.id.new_product_price);

        addButton = (Button) findViewById(R.id.new_product_add_button);

        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //action for user pick product image to upload
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(AddProductActivity.this);
            }
        });

        addButton.setOnClickListener(this);

        TextView titleTextview = (TextView) findViewById(R.id.titleTextView);

        isEditMode = getIntent().getBooleanExtra("isEditMode", false);
        if(isEditMode) {   //if edit mode(that means when admin click the edit button, then title will be chagned "Edit Product" and have to initialize all the edittexts
            titleTextview.setText("Edit Product");
            addButton.setText("Save Change");

            try {
                productName.setText(Common.currentProduct.productName);
                productSize.setText(Common.currentProduct.productSize);
                productPrice.setText(Common.currentProduct.productPrice);

                Picasso.with(this).load(Common.currentProduct.imageUrl).into(productImage);
            } catch (Exception e){
                e.printStackTrace();
            }

        }else {
            titleTextview.setText("Add New Product");
            addButton.setText("Add");
        }



    }


    ProgressDialog progressDialog;
    private void setupLoadingDlg(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait a second");
    }

    @Override
    public void onClick(View v) { // action to add new product to firebase

        final String pName = productName.getText().toString();
        final String pSize = productSize.getText().toString();
        final String pPrice = productPrice.getText().toString();

        if(!isEditMode && productImageUri == null){
            Toast.makeText(this, "Please select product image", Toast.LENGTH_SHORT).show();
            return;
        }

        if(pName.equals("")){
            Toast.makeText(this, "Please input the Product Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if(pSize.equals("")){
            Toast.makeText(this, "Please input the Product size", Toast.LENGTH_SHORT).show();
            return;
        }

        if(pPrice.equals("")){
            Toast.makeText(this, "Please input the Product price", Toast.LENGTH_SHORT).show();
            return;
        }


        String imageName = UUID.randomUUID().toString();
        StorageReference productRef = mStorageRef.child("products").child(imageName);




        final HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("productname", pName);
        data.put("size", pSize);
        data.put("price", pPrice);

        progressDialog.show();
        if(productImageUri != null) {
            productRef.putFile(productImageUri)  // image uploading action
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.hide();
                            // Get a URL to the uploaded content
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            Log.d(TAG, "onSuccess: " + downloadUrl);

                            data.put("picture", downloadUrl.toString());

                            // Write a message to the database

                            if (isEditMode) {
                                DatabaseReference myRef = database.getReference("items").child(Common.currentProduct.id);
                                myRef.setValue(data);
                                Toast.makeText(AddProductActivity.this, "Successfully updated product", Toast.LENGTH_SHORT).show();
                            } else {
                                DatabaseReference myRef = database.getReference("items").push().getRef();
                                myRef.setValue(data);
                                Toast.makeText(AddProductActivity.this, "Successfully saved new product!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            // ...
                            progressDialog.hide();
                            Toast.makeText(AddProductActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });
        }else{
            DatabaseReference myRef = database.getReference("items");
            myRef.child(Common.currentProduct.id).setValue(data);
            Toast.makeText(AddProductActivity.this, "Successfully updated product", Toast.LENGTH_SHORT).show();

        }
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {  //this will be invoked when user pick image
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                productImageUri = resultUri;
                Picasso.with(this).load(resultUri.toString()).into(productImage);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
