package au.edu.itc539csu.ecom;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class MainActivity extends AppCompatActivity {


    private TextView tvmain;
    SignInButton googleButton;
    FirebaseAuth firebaseAuthGoogleSignIn;
    GoogleApiClient thisAppGoogleApiClient;
    private final static int SIGN_IN_RC = 2;
    FirebaseAuth.AuthStateListener thisAppAuthListener;
    private EditText inputforEmail, inputPassword;
    private Button signInBtn, signUpBtn, passwordResetBtn, gotoAdminBtn;

    private FirebaseAuth firebaseAuthUserSignUp;

    @Override
    protected void onStart() {
        super.onStart();

        firebaseAuthGoogleSignIn.addAuthStateListener(thisAppAuthListener);
        thisAppGoogleApiClient.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        thisAppAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseauth){
                if( firebaseauth.getCurrentUser() != null){

//                    if(firebaseauth.getCurrentUser().getEmail().equals(Constant.adminMail)) {
//                        startActivity(new Intent(MainActivity.this, AdminActivity.class));
//                        finish();
//                    }else
                        startActivity(new Intent(MainActivity.this, AuthUserNotNull.class));
                }

              }
        };

        //Get Firebaseauth instance
        firebaseAuthUserSignUp = FirebaseAuth.getInstance();

        signInBtn = (Button) findViewById(R.id.button_signIn);
        signUpBtn = (Button) findViewById(R.id.button_sign_up);
        inputforEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.userPassword);

        passwordResetBtn = (Button) findViewById(R.id.btn_reset_password);

        passwordResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ResetPasswordActivity.class));
            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = inputforEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please enter your email id!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please enter your password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 5) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }


                //create user
                firebaseAuthUserSignUp.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(MainActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the authentication state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(MainActivity.this, "Authentication has failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    if(email.equals(Constant.adminMail)){
//                                        Intent intent = new Intent(MainActivity.this, AdminActivity.class);
//                                        startActivity(intent);
//                                        finish();
//                                        return;
                                    }

                                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                                    finish();
                                }
                            }
                        });
            }
        });



        gotoAdminBtn = (Button) findViewById(R.id.goto_admin_button);
        gotoAdminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("isAdminLogin", true);
                startActivity(intent);

            }
        });


        init();

    }

    public void init()
    {
        googleButton =(SignInButton)findViewById(R.id.google_btn);
        firebaseAuthGoogleSignIn = FirebaseAuth.getInstance();
        googleButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                signIn();

            }

        });


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        thisAppGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(MainActivity.this,"Something has gone wrong",Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }





    private void signIn() {  //sign in to app using google signin

        Intent IntentSignIn = Auth.GoogleSignInApi.getSignInIntent(thisAppGoogleApiClient);
        startActivityForResult(IntentSignIn, SIGN_IN_RC);
    }

    @Override
    public void onActivityResult(int codeRequest, int codeResult, Intent allData) {
        super.onActivityResult(codeRequest, codeResult, allData);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (codeRequest == SIGN_IN_RC) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(allData);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(MainActivity.this,"Authentication went wrong",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount googleSignInAccount) {
        AuthCredential credentials = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
        firebaseAuthGoogleSignIn.signInWithCredential(credentials)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "Sign in has been sucessful");
                            FirebaseUser user = firebaseAuthGoogleSignIn.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "Sign in failed", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });



    }


    @Override
    protected void onResume() {
        super.onResume();

    }

}
