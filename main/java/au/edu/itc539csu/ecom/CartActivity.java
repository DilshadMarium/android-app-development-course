package au.edu.itc539csu.ecom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import au.edu.itc539csu.ecom.adapters.admin.CartAdapter;
import au.edu.itc539csu.ecom.model.OpenCart;

public class CartActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CartAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);



        recyclerView = (RecyclerView) findViewById(R.id.cart_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CartAdapter(this);
        adapter.loadData();
        recyclerView.setAdapter(adapter);


        TextView totalPrice = (TextView) findViewById(R.id.cart_totalPrice);
        totalPrice.setText("Total = " + OpenCart.getTotalPrice() + " AUD");



        Button backButton = (Button) findViewById(R.id.cart_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button nextButton = (Button) findViewById(R.id.cart_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this, AddressActivity.class));
            }
        });




    }
}
