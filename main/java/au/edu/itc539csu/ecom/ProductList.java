package au.edu.itc539csu.ecom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import au.edu.itc539csu.ecom.adapters.admin.ProductListAdapter;
import au.edu.itc539csu.ecom.model.OpenCart;


public class ProductList extends AppCompatActivity {


    RecyclerView recyclerView;
    ProductListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        OpenCart.clearCart();

        recyclerView = (RecyclerView) findViewById(R.id.product_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ProductListAdapter(this);
        adapter.loadData();
        recyclerView.setAdapter(adapter);


        Button gotoCartButton = (Button) findViewById(R.id.product_next_button);
        gotoCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if(OpenCart.count() ==0 ){
                    Toast.makeText(ProductList.this, "Please add at least one item", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                startActivity(new Intent(ProductList.this, CartActivity.class));
            }
        });

    }
}
